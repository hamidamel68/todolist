import { TestBed, inject } from '@angular/core/testing';

import { TapsellstorageService } from './tapsellstorage.service';

describe('TapsellstorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TapsellstorageService]
    });
  });

  it('should be created', inject([TapsellstorageService], (service: TapsellstorageService) => {
    expect(service).toBeTruthy();
  }));
});
