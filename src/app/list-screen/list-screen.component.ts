import { Component, OnInit } from "@angular/core";
import { TapsellstorageService } from "../tapsellstorage.service";
import {
  MatDialog,
  MatSnackBar
} from "../../../node_modules/@angular/material";
import { List } from "../list";
import { DialogAddListComponent } from "../dialog-add-list/dialog-add-list.component";
import { Router } from "../../../node_modules/@angular/router";

@Component({
  selector: "app-list-screen",
  templateUrl: "./list-screen.component.html",
  styleUrls: ["./list-screen.component.css"]
})
export class ListScreenComponent implements OnInit {
  lists: any[];
  constructor(
    protected TapsellstorageService: TapsellstorageService,
    public dialog: MatDialog,
    private router: Router,
    public snackBar: MatSnackBar
  ) {}

  currentList: List = {
    _id: "",
    title: "",
    isMain: false
  };
  reload() {
    this.TapsellstorageService.getLists().subscribe(
      res => {
        console.log(res);
        this.lists = res;
      },
      err => {
        console.log(err);
      }
    );
  }
  editList(list) {
    this.currentList = list;
    const dialogRef = this.dialog.open(DialogAddListComponent, {
      width: "480px",
      data: this.currentList
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.currentList = result;
        this.TapsellstorageService.updateList(
          this.currentList._id,
          result
        ).subscribe(
          res => {
            let id = res["_id"];
            this.snackBar.open("لیست با موفقیت به روز شد", null, {
              duration: 3000
            });
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }
  viewTasks(list) {
    this.router.navigate(["/tasks", list._id]);
  }
  removeList(list) {
    this.TapsellstorageService.removeList(list).subscribe(
      res => {
        this.snackBar.open("لیست با موفقیت حذف شد", null, {
          duration: 3000
        });
        this.reload();
      },
      err => {
        console.log(err);
      }
    );
  }
  ngOnInit() {
    this.reload();
    this.TapsellstorageService.getListChangeEmitter().subscribe(item =>
      this.reload()
    );
  }
}
