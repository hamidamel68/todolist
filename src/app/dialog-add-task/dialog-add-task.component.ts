import { Component, OnInit, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Task } from "../task";
import { List } from "../list";


@Component({
  selector: "app-dialog-add-task",
  templateUrl: "./dialog-add-task.component.html",
  styleUrls: ["./dialog-add-task.component.css"]
})
export class DialogAddTaskComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogAddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {task:Task,list:List}
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
