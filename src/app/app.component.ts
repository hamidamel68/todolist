import { Component, OnInit } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";
import { DialogAddTaskComponent } from "./dialog-add-task/dialog-add-task.component";
import { Task } from "./task";
import { DialogAddListComponent } from "./dialog-add-list/dialog-add-list.component";
import { List } from "./list";
import { TapsellstorageService } from "./tapsellstorage.service";
import { ActivatedRoute } from "../../node_modules/@angular/router";
import { LocalStorage } from "../../node_modules/@ngx-pwa/local-storage";
import { Observable } from "../../node_modules/rxjs";
import { routerTransition } from "./router.transition";
import { map } from "rxjs/operators";
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState
} from "@angular/cdk/layout";
import { Directionality } from "../../node_modules/@angular/cdk/bidi";

@Component({
  selector: "app-root",
  animations: [routerTransition],
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  mainList: Observable<any>;

  title = "todolist";
  list: List = {
    _id: "",
    title: "",
    isMain: false
  };

  currentList: List = {
    _id: "",
    title: "",
    isMain: false
  };

  task: Task = {
    _id: undefined,
    title: "",
    date: undefined,
    description: "",
    list: undefined
  };

  constructor(
    public dir: Directionality,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    protected localStorage: LocalStorage,
    protected TapsellstorageService: TapsellstorageService,
    public snackBar: MatSnackBar,
    private breakpointObserver: BreakpointObserver
  ) {
    
    TapsellstorageService.getMainList().subscribe(res => {
      this.localStorage.setItem("mainList", res).subscribe(() => {
        //this.currentList=res;
      });
    });
  }

  newTaskDialog(): void {
    this.localStorage.getItem("currentList").subscribe(
      res => {
        this.currentList = res;

        const dialogRef = this.dialog.open(DialogAddTaskComponent, {
          width: "480px",
          data: { task: this.task, list: this.currentList }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.task = result.task;
            this.task.list = result.list._id;
            this.TapsellstorageService.addNewTask(this.task).subscribe(
              res => {
                this.TapsellstorageService.emitTaskChangeEvent(this.list);
                this.snackBar.open("تسک با موفقیت اضافه شد", null, {
                  duration: 3000
                });
                this.task = {
                  _id: undefined,
                  title: "",
                  date: undefined,
                  description: "",
                  list: undefined
                };
              },
              err => {
                console.log(err);
              }
            );
          }
        });
      },
      err => {
        console.log(err);
      }
    );
  }
  newListDialog(): void {
    const dialogRef = this.dialog.open(DialogAddListComponent, {
      width: "480px",
      data: this.list
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.list = result;
        this.list._id = undefined;
        this.TapsellstorageService.addNewList(this.list).subscribe(
          res => {
            let id = res["_id"];
            this.snackBar.open("لیست با موفقیت اضافه شد", null, {
              duration: 3000
            });

            this.TapsellstorageService.emitListChangeEvent(this.list);

            this.list = {
              _id: "",
              title: "",
              isMain: false
            };
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }

  ngOnInit() {}
}
