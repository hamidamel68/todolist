import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { LayoutModule } from "@angular/cdk/layout";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { TaskScreenResolver } from "./tasks-screen/tasks-screen.resolver";

const appRoutes: Routes = [
  { path: "lists", component: ListScreenComponent, data: { state: "lists" } },
  {
    path: "tasks/:id",
    data: { state: "tasks" },
    component: TasksScreenComponent,
    resolve: {
      list: TaskScreenResolver
    }
  },
  {
    path: "donelist",
    data: { state: "tasks" },
    component: DoneScreenComponent,
  },
  {
    path: "",
    data: { state: "defaultTasks" },
    component: TasksScreenComponent,
    resolve: {
      list: TaskScreenResolver
    }
  },
  { path: "**", redirectTo: "", pathMatch: "full" }
];

import {
  MatListModule,
  MatSelectModule,
  MatCardModule,
  MatSnackBarModule,
  MatButtonModule,
  MatIconModule,
  MatDatepickerModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatGridListModule,
  MatDialogModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatCheckboxModule,
  MatRippleModule,
  MatRadioModule
} from "@angular/material";
import { TaskComponent } from "./task/task.component";
import { TasksScreenComponent } from "./tasks-screen/tasks-screen.component";
import { DialogAddTaskComponent } from "./dialog-add-task/dialog-add-task.component";
import { DialogAddListComponent } from "./dialog-add-list/dialog-add-list.component";
import { ListScreenComponent } from "./list-screen/list-screen.component";
import { ListComponent } from "./list/list.component";

import { DoneScreenComponent } from './done-screen/done-screen.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    TasksScreenComponent,
    DialogAddTaskComponent,
    DialogAddListComponent,
    ListScreenComponent,
    ListComponent,
    DoneScreenComponent
  ],
  entryComponents: [DialogAddTaskComponent, DialogAddListComponent],

  imports: [
    MatListModule,
    MatCardModule,
    BrowserModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatSelectModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatSidenavModule,
    MatGridListModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatNativeDateModule,
    HttpClientModule,
    MatRadioModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [TaskScreenResolver],
  bootstrap: [AppComponent]
})
export class AppModule {}
