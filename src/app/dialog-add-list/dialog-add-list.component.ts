import { Component, OnInit, Inject } from '@angular/core';
import { List } from '../list';
import { DialogAddTaskComponent } from '../dialog-add-task/dialog-add-task.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '../../../node_modules/@angular/material';

@Component({
  selector: 'app-dialog-add-list',
  templateUrl: './dialog-add-list.component.html',
  styleUrls: ['./dialog-add-list.component.css']
})
export class DialogAddListComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogAddListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: List
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
