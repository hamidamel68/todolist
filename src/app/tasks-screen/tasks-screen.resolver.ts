import { Injectable, NgModule } from "../../../node_modules/@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  RouterModule
} from "../../../node_modules/@angular/router";
import { List } from "../list";
import { Observable } from "../../../node_modules/rxjs";
import { TapsellstorageService } from "../tapsellstorage.service";

@Injectable()
export class TaskScreenResolver implements Resolve<any> {
  constructor(private TapsellstorageService: TapsellstorageService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    if (route.params.id)
    return this.TapsellstorageService.getList(route.params.id);
    else{
      return this.TapsellstorageService.getMainList();
    }
  }
}
