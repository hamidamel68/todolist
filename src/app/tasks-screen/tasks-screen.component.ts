import { Component, OnInit } from "@angular/core";
import { TapsellstorageService } from "../tapsellstorage.service";
import { MatDialog, MatSnackBar } from "../../../node_modules/@angular/material";
import { DialogAddTaskComponent } from "../dialog-add-task/dialog-add-task.component";
import { Task } from "../task";
import { List } from "../list";
import { ActivatedRoute, Router } from "../../../node_modules/@angular/router";
import { LocalStorage } from "../../../node_modules/@ngx-pwa/local-storage";

@Component({
  selector: "app-tasks-screen",
  templateUrl: "./tasks-screen.component.html",
  styleUrls: ["./tasks-screen.component.css"]
})
export class TasksScreenComponent implements OnInit {
  tasks: any[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    protected localStorage: LocalStorage,
    protected TapsellstorageService: TapsellstorageService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  currentList: List = {
    _id: undefined,
    title: "",
    isMain: false
  };
  currentTask: Task = {
    _id: "",
    title: "",
    date: "",
    list: "",
    description: ""
  };

  reload() {
    this.TapsellstorageService.getTasks(this.currentList._id).subscribe(
      res => {
        console.log(res);
        this.tasks = res;
      },
      err => {
        console.log(err);
      }
    );
  }
  changeList(task) {
    this.TapsellstorageService.getMainList().subscribe(
      res => {
        task.list = res._id;
        this.TapsellstorageService.updateTask(task._id, task).subscribe(res => {
          this.snackBar.open("به لیست کارهای روزانه رفت", null, {
            duration: 3000
          });
          this.reload();
        });
      },
      err => {
        console.log(err);
      }
    );
  }
  toggleTask(task) {
    this.TapsellstorageService.updateTask(task._id, task).subscribe(
      res => {
        this.snackBar.open("تسک با موفقیت به روز شد", null, {
          duration: 3000
        });
      },
      err => {
        console.log(err);
      }
    );
  }
  editTask(task) {
    this.currentTask = task;
    const dialogRef = this.dialog.open(DialogAddTaskComponent, {
      width: "480px",
      data: { task: this.currentTask, list: this.currentList }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.currentTask = result.task;
        this.TapsellstorageService.updateTask(
          this.currentTask._id,
          this.currentTask
        ).subscribe(
          res => {
            this.snackBar.open("تسک با موفقیت به روز شد", null, {
              duration: 3000
            });
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }
  removeTask(task) {
    this.TapsellstorageService.removeTask(task).subscribe(
      res => {
        this.snackBar.open("تسک با موفقیت به روز شد", null, {
          duration: 3000
        });
        this.reload();
        
      },
      err => {
        console.log(err);
      }
    );
    // this.tasks=this.TapsellstorageService.getTasks();
  }
  ngOnInit() {
    this.currentList = this.route.snapshot.data.list;
    this.TapsellstorageService.getTaskChangeEmitter().subscribe(item =>
      this.reload()
    );
    this.localStorage.setItem("currentList", this.currentList).subscribe(() => {
      this.reload();
    });
  }
}
