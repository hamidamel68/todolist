import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.css"]
})
export class TaskComponent implements OnInit {
  _ref: any;

  @Input() task;

  @Output() remove = new EventEmitter();
  @Output() toggle = new EventEmitter();
  @Output() edit = new EventEmitter();
  @Output() change = new EventEmitter();
  changeList(){
    this.change.emit(this.task);
  }
  toggleTask() {
    
    this.toggle.emit(this.task);
  }
  removeTask() {
    this.remove.emit(this.task);
  }
  editTask() {
    this.edit.emit(this.task);
  }
  ngOnInit() {}
}
