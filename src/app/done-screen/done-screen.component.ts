import { Component, OnInit } from "@angular/core";
import { TapsellstorageService } from "../tapsellstorage.service";
import { MatDialog } from "../../../node_modules/@angular/material";
import { DialogAddTaskComponent } from "../dialog-add-task/dialog-add-task.component";
import { Task } from "../task";
import { List } from "../list";
import { ActivatedRoute, Router } from "../../../node_modules/@angular/router";
import { LocalStorage } from "../../../node_modules/@ngx-pwa/local-storage";

@Component({
  selector: "app-done-screen",
  templateUrl: "./done-screen.component.html",
  styleUrls: ["./done-screen.component.css"]
})
export class DoneScreenComponent implements OnInit {
  tasks: any[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    protected localStorage: LocalStorage,
    protected TapsellstorageService: TapsellstorageService,
    public dialog: MatDialog
  ) {}

 
  currentTask: Task = {
    _id: "",
    title: "",
    date: "",
    list: "",
    description: ""
  };

  reload() {
    this.TapsellstorageService.getDoneTasks().subscribe(
      res => {
        this.tasks = res;
      },
      err => {
        console.log(err);
      }
    );
  }
  
  toggleTask(task) {
    this.TapsellstorageService.updateTask(task._id, task).subscribe(
      res => {
        this.reload();
      },
      err => {
        console.log(err);
      }
    );
  }
  
  removeTask(task) {
    this.TapsellstorageService.removeTask(task).subscribe(
      res => {
        this.reload();
      },
      err => {
        console.log(err);
      }
    );
    // this.tasks=this.TapsellstorageService.getTasks();
  }
  ngOnInit() {
    this.reload()
    
    
  }
}
