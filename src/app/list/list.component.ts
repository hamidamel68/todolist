import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  _ref:any;   

  @Input() list;

  @Output()
  remove = new EventEmitter();
  @Output()
  edit= new EventEmitter();
  @Output()
  view= new EventEmitter();

  removeList(){
    this.remove.emit(this.list);
  }
  editList(){
    this.edit.emit(this.list);
  }
  viewList(){
    this.view.emit(this.list);
  }
  ngOnInit() {
  }

}
