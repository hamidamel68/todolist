import { Injectable, EventEmitter } from "@angular/core";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { List } from "./list";
import { Observable, of, throwError } from "rxjs";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { catchError, tap, map } from "rxjs/operators";
import { Task } from "./task";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
const apiUrl="http://localhost:3000/api/"
const apiTaskUrl = apiUrl+"tasks";
const apiListUrl = apiUrl+"lists";

@Injectable({
  providedIn: "root"
})
export class TapsellstorageService {
  constructor(protected localStorage: LocalStorage, private http: HttpClient) {}
  listChanged: EventEmitter<any[]> = new EventEmitter();
  taskChanged: EventEmitter<any[]> = new EventEmitter();
  emitListChangeEvent(any) {
    this.listChanged.emit(any);
  }
  getListChangeEmitter() {
    return this.listChanged;
  }
  emitTaskChangeEvent(any) {
    this.taskChanged.emit(any);
  }
  getTaskChangeEmitter() {
    return this.taskChanged;
  }
  lists = [];
  tasks = [];

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getList(id) {
    return this.http.get(`${apiListUrl}/${id}`, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  
  getMainList() : Observable<any> {
    return this.http.get(apiUrl+`mainList`, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    )
  }

  getDoneTasks(): Observable<any> {
    return this.http.get(apiUrl+`compeleted`, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getTasks(list): Observable<any> {
    return this.http.get(`${apiTaskUrl}/query/${list}`, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  getLists(): Observable<any> {
    return this.http.get(apiListUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  
  getTasksById(id): Observable<any> {
    const url = `${apiTaskUrl}/${id}`;
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  addNewTask(task): Observable<any> {
    return this.http
      .post(apiTaskUrl, task, httpOptions)
      .pipe(catchError(this.handleError));
  }

  addNewList(list): Observable<any> {
    return this.http
      .post(apiListUrl, list, httpOptions)
      .pipe(catchError(this.handleError));
  }

  removeTask(task:Task): Observable<{}> {
    const url = `${apiTaskUrl}/${task._id}`;
    return this.http
      .delete(url, httpOptions)
      .pipe(catchError(this.handleError));
  }

  removeList(list): Observable<{}> {
    const url = `${apiListUrl}/${list._id}`;
    return this.http
      .delete(url, httpOptions)
      .pipe(catchError(this.handleError));
  }

  updateTask(id, task): Observable<any> {
    const url = `${apiTaskUrl}/${id}`;

    return this.http
      .put(url, task, httpOptions)
      .pipe(catchError(this.handleError));
  }

  updateList(id, list): Observable<any> {
    const url = `${apiListUrl}/${id}`;

    return this.http
      .put(url, list, httpOptions)
      .pipe(catchError(this.handleError));
  }
}
