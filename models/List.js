var mongoose = require('mongoose');
var ListSchema = new mongoose.Schema({
    title: String,
    date: { type: Date, default: Date.now },
    isMain: Boolean
  });
  module.exports = mongoose.model('List', ListSchema);
